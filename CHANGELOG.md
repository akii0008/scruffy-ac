# Changelog

## 0.0.2 [2021/9/8]
* Start server / stop server capability from CLI client tool
  * `yarn start-client --start` - Starts the server without having to run `yarn pm2-start`
  * `yarn start-client --stop` - Stops the server without having to exit the pm2 process manually

## 0.0.1
* Ability to play the music lol
* Socket control- connect via the socket and play with the settings
* `yarn pm2-start` then one of the following:
  * `yarn start-client --help` - Displays available options
  * `yarn start-client --volume <#/%>` - Sets volume out of 100
  * `yarn start-client --mute` - Mutes playback
  * `yarn start-client --unmute` - Unmutes playback