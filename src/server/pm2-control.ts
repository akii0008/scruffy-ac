import pm2 from 'pm2';

console.log('starting server...');

pm2.connect(function (err) {
  if (err) {
    console.error(err);
    process.exit(2);
  }

  console.log('connected to pm2');
  pm2.start({
    script: 'build/server/index.js',
    name: 'scruffy-ac'
  }, function (err) {
    if (err) {
      console.error(err);
      return pm2.disconnect();
    }
  });
});

// exit this script after a delay because if we dont,
// pm2 doesn't register that we started the server.
setTimeout(() => process.exit(), 500);